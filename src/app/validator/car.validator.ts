import { AbstractControl,  ValidationErrors, ValidatorFn } from "@angular/forms"

export class CarValidator {

    public static veiculoValidator(control: AbstractControl): ValidationErrors | null {
        let veiculo = control.value.trim()
        let veiculoParts = veiculo.split('')

        if (veiculo == '' || veiculoParts.length >= 2) {
            return null;
        }
        return{
            veiculoInvalid: true
        }
    }

    

    
}