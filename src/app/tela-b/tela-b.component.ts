import { Component, OnInit } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { Car } from '../tela-a/model/car.model';

@Component({
    selector: 'app-tela-b',
    templateUrl: './tela-b.component.html',
    styleUrls: ['./tela-b.component.css']
})

export class TelaBComponent implements OnInit {

    allCar: Car[] = []

    constructor(private router: Router) { }

    goToA(): void {
        this.router.navigateByUrl('telaA')        
    }

    encerrar(): void {

    }

    

    ngOnInit(): void {
        var formString = localStorage.getItem('telaB') || ""
        this.allCar = JSON.parse(formString)
      }
}