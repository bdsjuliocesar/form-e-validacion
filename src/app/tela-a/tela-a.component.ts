import { Component, OnInit } from '@angular/core';
import { ReactiveFormsModule, FormBuilder,FormControl, FormGroup, Validators, AbstractControlOptions } from '@angular/forms';
import { Car } from './model/car.model';
import { Router } from '@angular/router';
import { CarValidator } from '../validator/car.validator';


@Component({
  selector: 'app-tela-a',
  templateUrl: './tela-a.component.html',
  styleUrls: ['./tela-a.component.css']
})
export class TelaAComponent implements OnInit {
  
  carForm = this.formBuilder.group({
    veiculo:   ['', [Validators.required], Validators.minLength(3), CarValidator],
    placa:     ['', [Validators.required, Validators.minLength(7)]],
    motorista: ['', [Validators.required]],
    telefone:  ['', [Validators.required]],
    })

  allCar: Car[] = []

  constructor(private formBuilder: FormBuilder, private router: Router){

  }

  ngOnInit() {
    let list = localStorage.getItem('telaB')
    if (list != null) {
      this.allCar = JSON.parse(list)
    }
  }

  goToB(): void {
    this.router.navigateByUrl('telaB')        
} 



  save() {
    
    let veiculo = this.carForm.value.veiculo
    let placa = this.carForm.value.placa
    let motorista = this.carForm.value.motorista
    let telefone = this.carForm.value.telefone
    
  
    let car = new Car(veiculo, placa, motorista, telefone)
    this.allCar.push(car)  
    
    var entrada = Date.now
  
    localStorage.setItem('telaB', JSON.stringify(this.allCar))
    this.router.navigateByUrl('telaB')
  }

  get veiculo() {
    return this.carForm.controls.veiculo
  }

  get placa() {
    return this.carForm.controls.placa
  }

  get motorista() {
    return this.carForm.controls.motorista
  }

  get telefone() {
    return this.carForm.controls.telefone
  }

}
