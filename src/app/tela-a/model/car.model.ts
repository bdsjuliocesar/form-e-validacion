export class Car{
    veiculo : string
    placa : string
    motorista : string
    telefone : string

    constructor(veiculo: string, placa: string, motorista: string, telefone: string){
        this.veiculo = veiculo
        this.placa = placa
        this.motorista = motorista
        this.telefone = telefone
    }
}